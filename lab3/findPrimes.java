package com.company;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class FindPrimes {

    public static void main(String[] args){
        int limit = Integer.parseInt(args[0]); //args is equal to 50
        // print the numbers less than limit
        // for each number less than limit
        for (int number = 2; number < limit ; number++) {
        // check if the number is prime or not
        // let divisor = 2;
            int divisor =2 ;
            // let isPrime == true;
            boolean isPrime = true;
            //while divisor less then number
            while (divisor < number && isPrime){
                // if the number divisible by divisor
                // number isn't prime
                // increment divisor
                if (number % divisor == 0){
                    isPrime = false;
                }
                divisor++;
            }
        // if it is prime
            if (isPrime){
                System.out.println(number + " ");
            }
           //print the numbers


        }




    }
}
