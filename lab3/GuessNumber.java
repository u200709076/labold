package com.company;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;


public class GuessNumber {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        Random rand = new Random();
        int number =rand.nextInt(100);
        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");
        int guess = reader.nextInt();
        // exercise 1:
//        if (number == guess) {
//            System.out.println("Congratulations!");
//        }
//        else {
//            System.out.println("Sorry! the number was "+ number);
//        }
        // exercise 2:
//        while (guess != -1) {
//            if (guess == number) {
//                System.out.println("Congratulations!");
//                break;
//            }
//            else {
//                System.out.println("Type -1 to quit or guess another: ");
//                guess = reader.nextInt();
//            }
//        }
//        System.out.println("Sorry, the number was "+number);
         // exercise 3:
//        while (guess != -1) {
//            if (guess == number) {
//                System.out.println("Congratulations!");
//                break;
//            } else if (guess > number) {
//                System.out.println("Sorry!");
//                System.out.println("Number is less than guess.");
//                System.out.println("Type -1 to quit or guess another: ");
//                guess = reader.nextInt();
//            } else {
//                System.out.println("Sorry!");
//                System.out.println("Number is greater than guess.");
//                System.out.println("Type -1 to quit or guess another: ");
//                guess = reader.nextInt();
//            }
//        }
        // exercise 4:
        int attemp = 1;
        while (guess != -1) {
            if (guess == number) {
                System.out.println("Congratulations! You won after "+attemp+" attemps!");
                break;
            } else if (number > guess) {
                System.out.println("Sorry!");
                System.out.println("Number is greater than guess.");
                System.out.println("Type -1 to quit or guess another: ");
                guess = reader.nextInt();
                ++attemp;
            } else {
                System.out.println("Sorry!");
                System.out.println("Number is less than guess.");
                System.out.println("Type -1 to quit or guess another: ");
                guess = reader.nextInt();
                ++attemp;
            }
        }
        reader.close(); //Close the resource before exiting
    }
}
