package com.company;

import java.util.Scanner;

public class FindGrade {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter your grade : ");
        int score = input.nextInt();
        if (score < 60){
            System.out.println("F");
        }
        else if (score < 70 && score >= 60){
            System.out.println("D");
        }
        else if (score < 80 && score >= 70){
            System.out.println("C");
        }
        else if (score < 90 && score >= 80){
            System.out.println("B");
        }
        else if (score >= 90 && score <= 100){
            System.out.println("A");
        }
    }
}

